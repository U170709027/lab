package stack;

public class TestStackImpl {

	public static void main(String[] args) {
		/*
		StackImpl stack = new StackImpl();
		
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		
		while(!stack.empty()) {
			System.out.println(stack.pop());
		}
	
		*/
		
		Stack stack = new StackArrayImpl();
	
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		
		while(!stack.empty()) {
			System.out.println(stack.pop());
		}
		
	
	
	}
}
